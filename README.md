# Books Web Portal App

A web app built as an educational project for social nerding between book readers.

## Build & Run

1. Clone the repo: `git clone --recursive`
2. `npm install` the server and the client.
3. `npm run serve` to run the client and `node index.js` for the server.

### Technologies

- [Node](https://nodejs.org/en/download/)
- [Vue.js](https://cli.vuejs.org/guide/installation.html)
